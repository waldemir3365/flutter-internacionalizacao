import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  bool isBusy = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text(AppLocalizations.of(context).helloWorld)),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(AppLocalizations.of(context).love,
                style: TextStyle(color: Theme.of(context).accentColor)),
            ElevatedButton(
              onPressed: () {},
              child: Text(AppLocalizations.of(context).save,
                  style: TextStyle(color: Theme.of(context).accentColor)),
            ),
            Switch(
              value: isBusy,
              onChanged: (value) {
                setState(() {
                  isBusy = value;
                });
              },
            ),
          ],
        ),
      ),
    );
  }
}
